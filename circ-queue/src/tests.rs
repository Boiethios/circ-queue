use super::*;

#[test]
fn macro_works() {
    let q = CircQueue::<i32>::with_capacity(3);

    assert!(q.get(0).is_none());
}

// push_back

#[test]
fn push_back_works_for_size_0() {
    let mut q = CircQueue::with_capacity(0);

    assert!(q.push_back(0).is_none());
    assert!(q.push_back(0).is_none());
}

#[test]
fn push_back_works_for_size_1() {
    let mut q = CircQueue::with_capacity(1);

    assert!(q.push_back(0).is_none());
    assert_eq!(q.push_back(1), Some(0));
    assert_eq!(q.push_back(2), Some(1));
}

#[test]
fn push_back_works_for_size_3() {
    let mut q = CircQueue::with_capacity(3);

    assert!(q.push_back(0).is_none());
    assert!(q.push_back(1).is_none());
    assert!(q.push_back(2).is_none());
    assert_eq!(q.push_back(3), Some(0));
    assert_eq!(q.push_back(4), Some(1));
}

// push_front

#[test]
fn push_front_works_for_size_0() {
    let mut q = CircQueue::with_capacity(0);

    assert!(q.push_front(0).is_none());
    assert!(q.push_front(0).is_none());
}

#[test]
fn push_front_works_for_size_1() {
    let mut q = CircQueue::with_capacity(1);

    assert!(q.push_front(0).is_none());
    assert_eq!(q.push_front(1), Some(0));
    assert_eq!(q.push_front(2), Some(1));
}

#[test]
fn push_front_works_for_size_3() {
    let mut q = CircQueue::with_capacity(3);

    assert!(q.push_front(0).is_none());
    assert!(q.push_front(1).is_none());
    assert!(q.push_front(2).is_none());
    assert_eq!(q.push_front(3), Some(0));
    assert_eq!(q.push_front(4), Some(1));
}

// change_capacity

/*
#[test]
fn change_capacity_works() {
    let mut q = CircQueue::with_capacity(3);
    
    q.push_back(0);
    q.push_back(1);
    q.push_back(2);
    assert_eq!(q.as_slices(), (&[0, 1, 2][..], &[][..]));

    q.change_capacity(4);
    assert_eq!(q.as_slices(), (&[0, 1, 2][..], &[][..]));
    assert!(q.push_back(3).is_none());
    assert_eq!(q.as_slices(), (&[0, 1, 2, 3][..], &[][..]));
    assert_eq!(q.push_back(4), Some(0));
    assert_eq!(q.as_slices(), (&[1, 2, 3, 4][..], &[][..]));
}
*/
