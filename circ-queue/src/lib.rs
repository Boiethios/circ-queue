#![feature(type_ascription)]
#![allow(dead_code)]

#[cfg(test)]
mod tests;
#[macro_use]
mod wrap;

/*
macro_rules! circ_queue {
    [ $( $e:expr ),* ; $size:expr ] => {{
        let mut q = CircQueue::with_capacity($size);

        $(
            q.pu$e,*
        )
    }}
}
*/

use std::collections::vec_deque::*;

#[derive(Debug)]
pub struct CircQueue<T> {
    vecdeque: VecDeque<T>,
    size: usize,
}

impl<T> CircQueue<T> {
    pub fn with_capacity(size: usize) -> Self {
        CircQueue {
            vecdeque: VecDeque::with_capacity(size + 1),
            size
        }
    }

    //pub fn change_capacity(&mut self, size: usize) -> Option<Vec<T>> {
    //    self.size = size;
    //}

    pub fn push_back(&mut self, value: T) -> Option<T> {
        let result = match self.vecdeque.len() + 1 > self.size {
            true => self.vecdeque.pop_front(),
            false => None,
        };
        if self.size != 0 {
            self.vecdeque.push_back(value);
        }
        result
    }

    pub fn push_front(&mut self, value: T) -> Option<T> {
        let result = match self.vecdeque.len() + 1 > self.size {
            true => self.vecdeque.pop_back(),
            false => None,
        };
        if self.size != 0 {
            self.vecdeque.push_front(value);
        }
        result
    }

    // reserve (a way to change the capacity)
    // insert(index: usize, value: T)
    // split_off
    // append

    wrap!(get(&self, index: usize) -> Option<&T>);
    wrap!(get_mut(&mut self, index: usize) -> Option<&mut T>);

    wrap!(swap(&mut self, i: usize, j: usize));
    wrap!(capacity(&self) -> usize);

    wrap!(iter(&self) -> Iter<T>);
    wrap!(iter_mut(&mut self) -> IterMut<T>);

    wrap!(as_slices(&self) -> (&[T], &[T]));
    wrap!(as_mut_slices(&mut self) -> (&mut [T], &mut [T]));

    wrap!(len(&self) -> usize);
    wrap!(is_empty(&self) -> bool);
    wrap!(clear(&mut self));
    wrap!(front(&self) -> Option<&T>);
    wrap!(front_mut(&mut self) -> Option<&mut T>);
    wrap!(back(&self) -> Option<&T>);
    wrap!(back_mut(&mut self) -> Option<&mut T>);
    wrap!(pop_front(&mut self) -> Option<T>);
    wrap!(pop_back(&mut self) -> Option<T>);
    wrap!(swap_remove_back(&mut self, index: usize) -> Option<T>);
    wrap!(swap_remove_front(&mut self, index: usize) -> Option<T>);
    wrap!(remove(&mut self, index: usize) -> Option<T>);

    pub fn contains(&self, x: &T) -> bool
        where T: PartialEq
    {
        self.vecdeque.contains(x)
    }

    pub fn retain<F>(&mut self, f: F)
        where F: FnMut(&T) -> bool
    {
        self.vecdeque.retain(f)
    }
    //wrap!();
    //wrap!();
    //wrap!();
    //wrap!();
    //wrap!();
    //wrap!();
    //wrap!();
    //wrap!();
}
