macro_rules! wrap {
    ( $name:ident( &self, $( $i:ident : $t:ty ),* ) -> $r:ty ) => (
        pub fn $name( &self, $( $i : $t ),* ) -> $r {
            self.vecdeque.$name(
                $( $i : $t ),*
            )
        }
    );
    ( $name:ident( &mut self, $( $i:ident : $t:ty ),* ) -> $r:ty ) => (
        pub fn $name( &mut self, $( $i : $t ),* ) -> $r {
            self.vecdeque.$name(
                $( $i : $t ),*
            )
        }
    );

    ( $name:ident( &self, $( $i:ident : $t:ty ),* ) ) => (
        pub fn $name( &self, $( $i : $t ),* ) {
            self.vecdeque.$name(
                $( $i : $t ),*
            )
        }
    );
    ( $name:ident( &mut self, $( $i:ident : $t:ty ),* ) ) => (
        pub fn $name( &mut self, $( $i : $t ),* ) {
            self.vecdeque.$name(
                $( $i : $t ),*
            )
        }
    );

    ( $name:ident( &self ) -> $r:ty ) => (
        pub fn $name( &self ) -> $r {
            self.vecdeque.$name()
        }
    );
    ( $name:ident( &mut self ) -> $r:ty ) => (
        pub fn $name( &mut self ) -> $r {
            self.vecdeque.$name()
        }
    );

    ( $name:ident( &mut self ) ) => (
        pub fn $name( &mut self ) {
            self.vecdeque.$name()
        }
    );
}
